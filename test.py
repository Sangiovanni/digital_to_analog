#!/usr/bin/python

import sys
import time
from mydevices import mcp4822

def main():
    
    mcp = mcp4822(1, 0)
    for arg in sys.argv[1:]:
        mcp.write(int(arg))
        time.sleep(3)
    mcp.close()
    



if __name__ == "__main__":
    main()          
            
            
    # python mcp.py 43 678
        