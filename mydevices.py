import time
import RPi.GPIO as GPIO

class mcp4822:
    
    def __init__(self, gain, analog_output):
        GPIO.setmode(GPIO.BCM)
        
        self.ck_line = 11
        self.cs_line = 22
        self.mosi_line = 10
        
        GPIO.setup(self.ck_line, GPIO.OUT)
        GPIO.setup(self.cs_line, GPIO.OUT)
        GPIO.setup(self.mosi_line, GPIO.OUT)
        
        GPIO.output(self.ck_line, GPIO.LOW)
        GPIO.output(self.cs_line, GPIO.HIGH)
        GPIO.output(self.mosi_line, GPIO.LOW)
        
        self.word = 0x1000
        if analog_output == 1:  # VoutB
            self.word = self.word  |  0x8000
        if gain == 1:  # gain 1x:
            self.word = self.word  | 0x2000
            
        print('setting: ' + hex(self.word))
            
    def write(self, data):
            #print (' original: '+ str(data) + ' -> ' + hex(data))
            
            dataout = self.word | data
            #print ('To send: ' + hex(dataout) + ' -> ' + bin(dataout))
            if data > 0xfff:
                print('To send: ' + str(data) + ' -> ' + hex(data) + ' Value to big!')
            else:
                print('To send: ' + str(data) + ' -> ' + hex(data) + '  Sended: ' + hex(dataout))
                #send data
                GPIO.output(self.cs_line, GPIO.LOW)
                for i in range(16):
                    time.sleep(0.000005)   #5us
                    if 0x8000 & dataout :
                       GPIO.output(self.mosi_line, GPIO.HIGH) 
                       #print('1')
                    else:
                       GPIO.output(self.mosi_line, GPIO.LOW) 
                       #print('0')
                    dataout = dataout << 1
                    time.sleep(0.000005)
                    GPIO.output(self.ck_line, GPIO.HIGH)
                    time.sleep(0.000005)
                    GPIO.output(self.ck_line, GPIO.LOW)
                GPIO.output(self.cs_line, GPIO.HIGH)
                time.sleep(0.000005)
                
    def close(self):
        GPIO.cleanup()
                
    
                   
            
            
            